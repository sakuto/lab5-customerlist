package com.example.customerlist;

import java.util.*;

import android.os.Bundle;
import android.view.*;
import android.widget.SimpleAdapter;
import android.app.ListActivity;
import android.content.Intent;

public class MainActivity extends ListActivity {
	
	
	List<Map<String,String>> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        data = this.getSampleData();
        SimpleAdapter adapter = new SimpleAdapter(this, data,
        R.layout.item, new String[] {"name", "phone","package"}, new int[] {R.id.tvName, R.id.tvPhone, R.id.tvPackage});
        this.setListAdapter(adapter); 
        
    }
        
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    
    
    List<Map<String,String>> getSampleData() {
    	List<Map<String,String>> l = new ArrayList<Map<String,String>>(); 
    	Map<String,String> m = new HashMap<String,String>(); 
    	m.put("name", "Bill␣Gate"); m.put("phone", "1-000-000-0000");m.put("package", "A"); l.add(m);
    	m = new HashMap<String,String>();
    	m.put("name", "Steve␣Jobs"); m.put("phone", "1-111-111-1111");m.put("package", "B"); l.add(m);
    	return l;
    	}
    public boolean onOptionsItemSelected(MenuItem item) {
    	int id = item.getItemId(); 
    	
    	switch(id) {
    	case R.id.action_new:
    		
    		Intent i = new Intent(this, Addnew.class); 
    		//i.putExtra("exchangeRate", exchangeRate); 
    		startActivityForResult(i, 9999);
    		
    	
    		return true;
    		
    		
    	default:
    	return super.onOptionsItemSelected(item);
    	} 
    	
    
    }
    protected void onActivityResult(int requestCode, int resultCode,
    		Intent dataresult) {
    		if (requestCode == 9999 && resultCode == RESULT_OK) {
    			String a,b,c;
    			
    			a = dataresult.getStringExtra("name");
    		    b = dataresult.getStringExtra("phone");
    			c = dataresult.getStringExtra("package");
    		
    			
    			
    			Map<String,String> m = new HashMap<String,String>(); 
		    	m.put("name", a);
		    	m.put("phone", b);
		    	m.put("package", c);
		    	data.add(m);
		    	SimpleAdapter adapter = (SimpleAdapter)this.getListAdapter(); 
		    	adapter.notifyDataSetChanged();
		    	
    		}
    
}
}
